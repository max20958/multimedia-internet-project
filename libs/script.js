$(function() {
    var canvas = document.getElementById("myCanvas");
    var ctx = canvas.getContext("2d");
    var x = canvas.width / 2;
    var y = canvas.height - 41;
    var dx = 2; //移動距離X
    var dy = 2; //移動距離Y
    var ballRadius = 10; //球的半徑
    var gameLevels = 1;

    var bomb_x = getRandom(0, 540);
    var bomb_y = -100;
    var bomb_dx = 0;
    var bomb_dy = 2;
    var bombRadius = 7; //球的半徑

    var paddleHeight = 10; //球拍H
    var paddleWidth = 75; //球拍W
    var paddleX = (canvas.width - paddleWidth) / 2;
    var rightPressed = false;
    var leftPressed = false;
    document.addEventListener("keydown", keyDownHandler, false);
    document.addEventListener("keyup", keyUpHandler, false);
    document.addEventListener("mousemove", mouseMoveHandler, false);

    var img = document.getElementById("bkSky");

    // var player = {
    //     name : "",
    //     score : 0,
    // };

    var playerName = "";
    var playerResults = localStorage.getItem('player-result') ? JSON.parse(localStorage.getItem('player-result')) : []

    localStorage.setItem('player-result', JSON.stringify(playerResults));
    var results = JSON.parse(localStorage.getItem('player-result'));


    //brick
    var brickRowCount = 3;
    var brickColumnCount = 5;
    var brickWidth = 87;
    var brickHeight = 20;
    var brickPadding = 10;
    var brickOffsetTop = 30;
    var brickOffsetLeft = 30;

    //setting
    var score = 0;
    var lives = 1;

    //use mouse to play
    function mouseMoveHandler(e) {
        var relativeX = e.clientX - canvas.offsetLeft;
        if (relativeX > 0 && relativeX < canvas.width) {
            paddleX = relativeX - paddleWidth / 2;
        }
    }

    //Show Score
    function drawScore() {
        ctx.font = "16px Arial";
        ctx.fillStyle = "#0095DD";
        ctx.fillText("Score: " + score, 8, 20);
    }

    // Show game Level
    function drawGameLevel() {
        ctx.font = "16px Arial";
        ctx.fillStyle = "#0095DD";
        ctx.fillText("Level: " + gameLevels, canvas.width - 65, 20);
    }

    // Create brick
    var bricks = [];

    function createBricks() {


        for (c = 0; c < brickColumnCount; c++) {
            bricks[c] = [];
            for (r = 0; r < brickRowCount; r++) {
                var randomPercentage = Math.random();

                if (gameLevels === 1) {
                    bricks[c][r] = { x: 0, y: 0, status: 1, level: 1 };
                } else if (gameLevels >= 2 && gameLevels <= 3) {
                    if (randomPercentage < 0.4) {
                        // 40% chance of being here
                        bricks[c][r] = { x: 0, y: 0, status: 1, level: 2 };
                    }else {
                        // 60% chance of being here
                        bricks[c][r] = { x: 0, y: 0, status: 1, level: 1 };
                    }
                } else if (gameLevels >= 4 && gameLevels <= 6) {
                    if (randomPercentage < 0.2) {
                        bricks[c][r] = { x: 0, y: 0, status: 1, level: 2 };
                    }else if (randomPercentage < 0.2) {
                        bricks[c][r] = { x: 0, y: 0, status: 1, level: 3 };
                    } else if (randomPercentage < 0.3) {
                        bricks[c][r] = { x: 0, y: 0, status: 1, level: 4 };
                    } else {
                        bricks[c][r] = { x: 0, y: 0, status: 1, level: 1 };
                    }
                } else if (gameLevels > 6) {
                    if (randomPercentage < 0.4) {
                        bricks[c][r] = { x: 0, y: 0, status: 1, level: 3 };
                    }else if (randomPercentage < 0.4) {
                        bricks[c][r] = { x: 0, y: 0, status: 1, level: 4 };
                    }else {
                        bricks[c][r] = { x: 0, y: 0, status: 1, level: 2 };
                    }
                }

            }
        }
    }

    createBricks()

    // 磚塊
    function drawBricks() {
        for (var c = 0; c < brickColumnCount; c++) {
            for (var r = 0; r < brickRowCount; r++) {
                if (bricks[c][r].status == 1) {
                    var brickX = (c * (brickWidth + brickPadding)) + brickOffsetLeft;
                    var brickY = (r * (brickHeight + brickPadding)) + brickOffsetTop;
                    bricks[c][r].x = brickX;
                    bricks[c][r].y = brickY;
                    ctx.beginPath();
                    ctx.rect(brickX, brickY, brickWidth, brickHeight);

                    switch (bricks[c][r].level) {
                        case 1 : ctx.fillStyle = "#0095DD";
                        break;
                        case 2 : ctx.fillStyle = "#00689a";
                            break;
                        case 3 : ctx.fillStyle = "#004a6e";
                            break;
                        case 4 : ctx.fillStyle = "#002c42";
                            break;
                    }

                    ctx.fill();
                    ctx.closePath();
                }
            }
        }
    }

    function checkEmptyBrick() {
        for (var c = 0; c < brickColumnCount; c++) {
            for (var r = 0; r < brickRowCount; r++) {
                if (bricks[c][r].status === 1) {
                    return false;
                }
            }
        }

        return true;
    }

    // 磚塊碰撞偵測 A collision detection function
    function collisionDetection() {
        for (var c = 0; c < brickColumnCount; c++) {
            for (var r = 0; r < brickRowCount; r++) {
                var b = bricks[c][r];
                if (b.status === 1) {
                    if (x > b.x && x < b.x + brickWidth && y > b.y && y < b.y + brickHeight) {
                        dy = -dy;
                        // dx = -dx;
                        switch (b.level) {
                            case 5:
                                b.level = 4;
                                break;
                            case 4:
                                b.level = 3;
                                break;
                            case 3:
                                b.level = 2;
                                break;
                            case 2:
                                b.level = 1;
                                break;
                            case 1:
                                score++;
                                b.status = 0
                                $('.textScore').text(score);
                                break;
                        }

                        console.log(b.level)
                        if (checkEmptyBrick()) {
                            setTimeout(function() {
                                gameLevels++;

                                dx += 0.5;
                                dy += 0.5;
                                bomb_dy += 0.5;
                                createBricks();
                            }, 1500)
                        }
                    }
                }
            }
        }
    }

    //Ball
    function drawBall() {
        ctx.beginPath();
        ctx.arc(x, y, ballRadius, 0, Math.PI * 2);
        ctx.fillStyle = "#0095DD";
        ctx.fill();
        ctx.closePath();
    }

    //Bomb
    function drawBomb() {
        ctx.beginPath();
        ctx.arc(bomb_x, bomb_y, bombRadius, 0, Math.PI * 2);
        ctx.fillStyle = "#ff2d2d";
        ctx.fill();
        ctx.closePath();

        if (bomb_y > 320) {
            //Bomb edge detection
            if (bomb_x > paddleX && bomb_x < paddleX + paddleWidth) {
                lives = lives - 1;
                if (!lives) {
                    playerResults.push({
                        name: playerName,
                        score: score
                    })
                    localStorage.setItem('player-result', JSON.stringify(playerResults))

                    alert("BOM !");
                    document.location.reload();
                }
            } else {
                bomb_y = 0;
                bomb_x = getRandom(0, 480);
            }
        }

    }

    //球拍
    function drawPaddle() {
        ctx.beginPath();
        ctx.rect(paddleX, canvas.height - paddleHeight, paddleWidth, paddleHeight);
        ctx.fillStyle = "#0095DD";
        ctx.fill();
        ctx.closePath();
    }

    function draw() {
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.drawImage(img, 0, 0); //background
        drawBricks();
        drawBall();
        drawBomb();

        bomb_x += bomb_dx;
        bomb_y += bomb_dy;

        drawPaddle();
        collisionDetection();
        drawScore();
        drawGameLevel();
        checkEmptyBrick();

        //邊緣偵測  bounce off the walls
        if (x + dx > canvas.width - ballRadius || x + dx < ballRadius) { //left and right
            dx = -dx;
        }
        if (y + dy < ballRadius) { //top
            dy = -dy;
        } else if (y + dy > canvas.height - ballRadius) {
            if (x > paddleX && x < paddleX + paddleWidth) { // collision detection between the ball and the paddle
                if (y = y - paddleHeight) {
                    dy = -dy;
                }
            }
            else { //bottom (game over)
                lives--;
                if (!lives) {
                    playerResults.push({
                        name: playerName,
                        score: score
                    })
                    localStorage.setItem('player-result', JSON.stringify(playerResults))

                    alert("GAME OVER");
                    document.location.reload();
                }
                else {
                    lives = lives - 1;
                    if (!lives) {
                        playerResults.push({
                            name: playerName,
                            score: score
                        })
                        localStorage.setItem('player-result', JSON.stringify(playerResults))

                        alert("GAME OVER");
                        document.location.reload();
                    }
                    else {
                        x = canvas.width / 2;
                        y = canvas.height - 30;
                        dx = 2;
                        dy = -2;
                        paddleX = (canvas.width - paddleWidth) / 2;
                    }
                }
            }

        }

        //球拍移動 7pixels
        if (rightPressed && paddleX < canvas.width - paddleWidth) {
            paddleX += 7;
        }
        else if (leftPressed && paddleX > 0) {
            paddleX -= 7;
        }

        x += dx;
        y += dy;

        requestAnimationFrame(draw);
    }

    function getRandom(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    };

    //按下 39 =  Right arrow , 37 =  Left arrow
    function keyDownHandler(e) {
        if (e.keyCode == 39) {
            rightPressed = true;
        }
        else if (e.keyCode == 37) {
            leftPressed = true;
        }
    }

    //放開
    function keyUpHandler(e) {
        if (e.keyCode == 39) {
            rightPressed = false;
        }
        else if (e.keyCode == 37) {
            leftPressed = false;
        }
    }

    $('#startButton').on('click', function(e) {
        e.preventDefault();

        if (!$('#player-name').val()) {
            alert("Please enter your name before start game!")
        } else {
            playerName = $('#player-name').val()
            $('#background-sky').css('display', 'none');

            draw();
        }
    })

    results.sort(function(a, b){
        return b.score - a.score
    })


    var html = "<table class='table'>";
    html+="<tr>";
    html+="<th>Play name</th>";
    html+="<th>Score</th>";

    html+="</tr>";
    for (var i = 0; i < results.length; i++) {
        if (i === 3) {
            break;
        }

        html+="<tr>";
        html+="<td>"+results[i].name+"</td>";
        html+="<td>"+results[i].score+"</td>";

        html+="</tr>";

    }
    html+="</table>";
    $("#play-result-table").html(html);
})